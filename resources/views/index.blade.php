@extends('web.layout.master')
@section('title')
    <title>Bất động sản Bảo Cường</title>
@stop
@section('content')
    @php
        $home = getSlider('trang-chu')->first();
    @endphp
    <div class="hero-section section">

        <div class="hero-slider hero-slider-one">
            <div class="hero-slide-item" style="background-image: url({{ $home->image }})">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <div class="hero-content ">
                                <h1>{{ $home->name }}</h1>
                                <h3 class="text-white mt-15">{{ $home->description }}</h3>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- Hero Section End -->


    <!-- Featured Properites Start -->
    <div class="featured-properites-section section pt-65 pb-75 pt-md-55 pb-md-55 pt-sm-45 pb-sm-45 pt-xs-25 pb-xs-25">
        <div class="container">

            <div class="row">
                <div class="section-title text-center col mb-30 mb-md-20 mb-xs-20 mb-sm-20">
                    <h2>Dự án nổi bật</h2>
                    <p> Những dự án bất động sản nổi bật. Bạn <br> có thể tìm kiếm và chọn cho mình những dự án tốt nhất</p>
                </div>
            </div>

            <div class="row">
                @foreach(getHotProject() as $project)
                    <div class="col-lg-3 col-md-6 col-12">
                        <!-- single-property Start -->
                        <div class="single-property mt-30">
                            <div class="property-img">
                                <a href="{{ route('project.detail',[$project->slug]) }}">
                                    <img src="{{ $project->image }}" alt="">
                                </a>
                            </div>
                            <div class="property-desc">
                                <h4><a href="{{ route('project.detail',[$project->slug]) }}">{{ $project->name }}</a></h4>
                                <p>
                                    <span class="location">{{ $project->location }}</span>
                                    <span class="property-info">{{ $project->description }}</span>
                                </p>
                                <div class="price-box">
                                    <p>Giá: {{ $project->price }}</p>
                                </div>
                            </div>
                        </div><!-- single-property End -->
                    </div>
                @endforeach
            </div>

        </div>
    </div><!-- Featured Properites End -->


    <!-- Featured Properites Start -->
    <div class="featured-properites-section section">
        <div class="container">

            <div class="row">
                <div class="section-title text-center col mb-30 mb-md-20 mb-xs-20 mb-sm-20">
                    <h2>Nhà Đất Bán</h2>
                </div>
            </div>

            <div class="row">
                @foreach(getLastLandLimit(4) as $land)
                    <div class="col-lg-3 col-md-6 col-12">
                        <!-- single-property Start -->
                        <div class="single-property mt-30">
                            <div class="property-img">
                                <a href="{{ route('land.detail',[$land->slug]) }}">
                                    <img src="{{ $land->image }}" alt="">
                                </a>
                            </div>
                            <div class="property-desc">
                                <h4><a href="{{ route('land.detail',[$land->slug]) }}">{{ $land->name }}</a></h4>
                                <p>
                                    <span class="location">{{ $land->location }}</span>
                                    <span class="property-info">{{ $land->address }} </span>
                                </p>
                                <div class="price-box">
                                    <p>{{ $land->price }}</p>
                                </div>
                            </div>
                        </div><!-- single-property End -->
                    </div>
                @endforeach
            </div>

        </div>
    </div><!-- Featured Properites End -->

    <!-- Ortiz Banner Area Start-->
    <div class="ortiz-banner-area section pt-110 pt-md-90 pt-sm-70 pt-xs-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="banner-inner-box">
                        <img src="assets/images/banner/banner-01.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Ortiz Banner Area End-->


    <!-- Our Agents Section Start -->
    <div class="our-agents-section section pt-110 pt-md-90 pt-sm-70 pt-xs-60 pb-110 pb-md-40 pb-sm-40 pb-xs-20">
        <div class="container">

            <div class="row">
                <div class="section-title text-center col mb-30 mb-md-20 mb-xs-20 mb-sm-20">
                    <h2>Our Agents</h2>
                    <p> one of the most popular real estate company all around USA. You
                        <br> can find your dream property or build property with us</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-6 mt-30">
                    <!-- Our Agents Start -->
                    <div class="our-agents">
                        <div class="agents-image">
                            <img src="assets/images/agents/agents-01.jpg" alt="">

                            <div class="agents-info">
                                <h3>View Details</h3>
                                <div class="agents-social">
                                    <ul>
                                        <li><a href="https://www.skype.com/en/"><i class="fa fa-skype"></i></a></li>
                                        <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="agents-contents">
                            <h4>Jassica Thomson</h4>
                            <p>Real Estate Agent</p>
                        </div>
                    </div><!-- Our Agents End -->
                </div>

                <div class="col-lg-3 col-md-6 mt-30">
                    <!-- Our Agents Start -->
                    <div class="our-agents">
                        <div class="agents-image">
                            <img src="assets/images/agents/agents-06.jpg" alt="">

                            <div class="agents-info">
                                <h3>View Details</h3>
                                <div class="agents-social">
                                    <ul>
                                        <li><a href="https://www.skype.com/en/"><i class="fa fa-skype"></i></a></li>
                                        <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="agents-contents">
                            <h4>Thomas Eilliams</h4>
                            <p>Real Estate Agent</p>
                        </div>
                    </div><!-- Our Agents End -->
                </div>

                <div class="col-lg-3 col-md-6 mt-30">
                    <!-- Our Agents Start -->
                    <div class="our-agents">
                        <div class="agents-image">
                            <img src="assets/images/agents/agents-03.jpg" alt="">

                            <div class="agents-info">
                                <h3>View Details</h3>
                                <div class="agents-social">
                                    <ul>
                                        <li><a href="https://www.skype.com/en/"><i class="fa fa-skype"></i></a></li>
                                        <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="agents-contents">
                            <h4>Sayana Sarlin</h4>
                            <p>Real Estate Agent</p>
                        </div>
                    </div><!-- Our Agents End -->
                </div>

                <div class="col-lg-3 col-md-6 mt-30">
                    <!-- Our Agents Start -->
                    <div class="our-agents">
                        <div class="agents-image">
                            <img src="assets/images/agents/agents-04.jpg" alt="">

                            <div class="agents-info">
                                <h3>View Details</h3>
                                <div class="agents-social">
                                    <ul>
                                        <li><a href="https://www.skype.com/en/"><i class="fa fa-skype"></i></a></li>
                                        <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="https://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="agents-contents">
                            <h4>Kuddus Boyati</h4>
                            <p>Real Estate Agent</p>
                        </div>
                    </div><!-- Our Agents End -->
                </div>

            </div>

        </div>

    </div><!-- Our Agents Section End -->


    <!-- Testimonial Section Start -->
    <div class="testimonial-section section pt-90 pt-md-70 pt-xs-60 pt-sm-70 testimonial-bg">
        <div class="container">
            <div class="row testimonial-slider section-mb-inner">
                <div class="col-lg-6">
                    <div class="single-testimonial">
                        <div class="testimonial-author">
                            <div class="image">
                                <img src="assets/images/testimonial/outher-01.jpg" alt="">
                            </div>
                            <div class="outhor-info">
                                <h4>Lora Momen Smith</h4>
                                <p>CEO, Momens Group</p>
                            </div>
                        </div>
                        <div class="testimonial-dec">
                            <p>one of the most popular real estate company all around USA. You can find your dream property or the build erty with us.
                                We always provide importance</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-testimonial">
                        <div class="testimonial-author">
                            <div class="image">
                                <img src="assets/images/testimonial/outher-02.jpg" alt="">
                            </div>
                            <div class="outhor-info">
                                <h4>Zakuline Fernandez </h4>
                                <p>CEO, Momens Group</p>
                            </div>
                        </div>
                        <div class="testimonial-dec">
                            <p>one of the most popular real estate company all around USA. You can find your dream property or the build erty with us.
                                We always provide importance</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-testimonial">
                        <div class="testimonial-author">
                            <div class="image">
                                <img src="assets/images/testimonial/outher-01.jpg" alt="">
                            </div>
                            <div class="outhor-info">
                                <h4>Lora Momen Smith</h4>
                                <p>CEO, Momens Group</p>
                            </div>
                        </div>
                        <div class="testimonial-dec">
                            <p>Ortiz is one of the most popular real estate company all around USA. You can find your dream property or the build erty
                                with us. We always provide importance</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Testimonial Section End -->


    <!-- Latest Blog Section Start -->
    <div class="latest-blog-section section pt-160 pt-md-140 pt-sm-120 pt-xs-110 pb-110 pb-md-90 pb-sm-70 pb-xs-60">
        <div class="container">


            <div class="row">
                <div class="section-title text-center col mb-30 mb-md-20 mb-xs-20 mb-sm-20">
                    <h2>Tin tức mới nhất</h2>
                    <p> Tin tức bất động sản mới nhất. Cập nhật <br> thường xuyên các tin tức nóng hổi
                    </p>
                </div>
            </div>


            <div class="row">
                @foreach(getLastBlogLimit() as $blog)
                    <div class="col-lg-4 col-md-6 mt-30">
                        <!-- Single latest blog Start -->
                        <div class="single-latest-blog">
                            <div class="latest-blog-image">
                                <a href="{{ route('post.detail',[$blog->slug]) }}"><img src="{{ $blog->image }}" alt=""></a>
                            </div>
                            <div class="latest-blog-contents">
                                <h4><a href="{{ route('post.detail',[$blog->slug]) }}">{{ $blog->name }}</a></h4>
                                <p><span>{{ $blog->created_at->format('d/m/Y' ) }}</span> / <span>{{ $blog->created_at->format('H:i') }}</span></p>
                                <p>{{ $blog->description }} </p>
                                <a class="read-more" href="{{ route('post.detail',[$blog->slug]) }}">Đọc tiếp</a>
                            </div>
                        </div><!-- Single latest blog End -->
                    </div>
                @endforeach
            </div>
        </div>
    </div><!-- Latest Blog Section End -->


@stop

