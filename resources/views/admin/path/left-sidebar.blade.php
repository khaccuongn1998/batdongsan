<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile dropdown m-t-20">
                        <div class="user-pic">
                            <img src="{{ asset('admin/assets/images/users/1.jpg') }}" alt="users" class="rounded-circle img-fluid"/>
                        </div>
                        <div class="user-content hide-menu m-t-10">
                            <h5 class="m-b-10 user-name font-medium">{{ auth()->user()->name }}</h5>
                            <a href="javascript:void(0)" class="btn btn-circle btn-sm m-r-5" id="Userdd" role="button" data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false">
                                <i class="ti-settings"></i>
                            </a>
                            <a href="{{ route('admin.logout') }}" title="Logout" class="btn btn-circle btn-sm">
                                <i class="ti-power-off"></i>
                            </a>
                            <div class="dropdown-menu animated flipInY" aria-labelledby="Userdd">
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                            </div>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>
                <!-- User Profile-->
                <li class="nav-small-cap">
                    <i class="mdi mdi-dots-horizontal"></i>
                    <span class="hide-menu">Personal</span>
                </li>
                @php
                    $menuItems = config('menu');
                @endphp
                @foreach($menuItems as $item)
                    <li class="sidebar-item">
                        <a class="sidebar-link {{ empty($item['items']) ? '' : 'has-arrow' }} waves-effect waves-dark"
                           href="{{ Route::has($item['route']) ? route($item['route']) : '1#'}}"
                           aria-expanded="false">
                            <i class="{{ $item['icon'] }}"></i>
                            <span class="hide-menu">{{ $item['name'] }}</span>
                        </a>
                        @if(!empty($item['items']))
                            <ul aria-expanded="false" class="collapse  first-level">
                                @foreach($item['items'] as $subItem)
                                <li class="sidebar-item">
                                    <a href="{{ Route::has($subItem['route']) ? route($subItem['route']) : '1#'}}" class="sidebar-link">
                                        <i class="{{ $subItem['icon'] }}"></i>
                                        <span class="hide-menu">{{ $subItem['name'] }}</span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
