@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Danh mục đất bán','key'=>'Thêm'])
@stop
@section('content')

    <div class="row">
        <!-- Column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.land.create') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <h5 class="card-title">Dự Án</h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Tên Bài Dự Án</label>
                                        <input type="text" name="name" id="firstName" class="form-control" placeholder="Nhập tên bài viết"></div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="card-title m-t-20">Upload Hình Ảnh</h5>
                                    <div class="btn btn-info waves-effect waves-light"><span>Upload Hình Ảnh khác</span>
                                        <input type="file" name="image" class="upload"></div>
                                </div>
                            </div>
                            <hr>
                            <!--/row-->
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Danh mục dự án</label>
                                        <select class="form-control" name="land_category_id" data-placeholder="Chọn Danh mục" tabindex="1">
                                            @foreach( getAllLandCategory() as $cate)
                                                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5 class="card-title m-t-40">THÔNG TIN CHUNG</h5>
                                    <div class="table-responsive">
                                        <table class="table table-bordered td-padding">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input type="text" name="area" class="form-control"
                                                           placeholder="Diện tích">
                                                </td>
                                                <td>
                                                    <input type="text" name="price" class="form-control"
                                                           placeholder="Giá">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" name="address"
                                                           class="form-control" placeholder="Địa chỉ">
                                                </td>
                                                <td>
                                                    <input type="text" name="location" class="form-control"
                                                           placeholder="Khu vực">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <h5 class="card-title m-t-40">Mô tả</h5>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <textarea class="form-control" name="description" rows="4" placeholder="Mô tả"></textarea>
                                    </div>
                                </div>
                            </div>

                            <h5 class="card-title m-t-40">Nội dung dự án</h5>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <textarea class="form-control" name="content" id="content-editor" rows="10" placeholder="Nội dung bài
                                        viết"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions m-t-40">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                            <button type="button" class="btn btn-dark">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
@stop
