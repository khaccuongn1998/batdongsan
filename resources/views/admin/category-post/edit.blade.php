@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Danh mục Bài viết','key'=>'Sửa'])
@stop
@section('content')

<div class="row">
    <!-- Column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin.category-post.update',[$result->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <h5 class="card-title">Thêm Danh Mục Tin Tức</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tên Danh Mục</label>
                                    <input type="text" name="name"
                                           id="firstName" class="form-control"
                                           value="{{ $result->name }}"
                                           placeholder="Nhà Đất..." required> </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Slug</label>
                                    <input type="text" name="slug"
                                           id="lastName" class="form-control"
                                           value="{{ $result->slug }}"
                                           placeholder="nha-dat"> </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <!--/row-->
                        <div class="row">
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Trạng Thái</label>
                                    <br/>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="is_active" value="1"
                                               id="customRadioInline1" name="customRadioInline1"
                                               {{ $result->is_active === 1 ? 'checked' : 0  }}
                                               class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline1">Kích Hoạt</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" name="is_active" value="0"
                                               id="customRadioInline2" name="customRadioInline1"
                                               {{ $result->is_active === 0 ? 'checked' : 0  }}
                                               class="custom-control-input">
                                        <label class="custom-control-label" for="customRadioInline2">Không Kích Hoạt</label>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                    </div>
                    <div class="form-actions m-t-40">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Sửa</button>
                        <button type="button" class="btn btn-dark">Huỷ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
@stop
