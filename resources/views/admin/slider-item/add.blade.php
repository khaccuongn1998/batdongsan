@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Slider','key'=>'Thêm'])
@stop
@section('content')

    <div class="row">
        <!-- Column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.slider-item.create') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <h5 class="card-title">Slider</h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Slider</label>
                                        <input type="text" name="name" id="firstName" class="form-control" placeholder="Nhập tên slider"></div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="card-title m-t-20">Upload Hình Ảnh</h5>
                                    <div class="btn btn-info waves-effect waves-light"><span>Upload Hình Ảnh khác</span>
                                        <input type="file" name="image" class="upload"></div>
                                </div>
                            </div>
                            <hr>
                            <!--/row-->
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Danh mục slider</label>
                                        <select class="form-control" name="slider_id" data-placeholder="Chọn Danh mục" tabindex="1">
                                            @foreach( getAllSliderCategory() as $cate)
                                                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h5 class="card-title m-t-40">Mô tả</h5>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <textarea class="form-control" name="description" rows="4" placeholder="Mô tả"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Trạng Thái</label>
                                        <br/>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="is_active" value="1" checked id="customRadioInline1" name="customRadioInline1"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline1">Kích Hoạt</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="is_active" value="0" id="customRadioInline2" name="customRadioInline1"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline2">Không Kích Hoạt</label>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <hr>
                            <div class="form-actions m-t-40">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-dark">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
@stop
