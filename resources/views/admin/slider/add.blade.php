@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Danh mục slider','key'=>'Thêm'])
@stop
@section('content')

<div class="row">
    <!-- Column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin.slider.create') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <h5 class="card-title">Thêm Danh Mục Slider</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Tên Danh Mục</label>
                                    <input type="text" name="name" id="firstName" class="form-control" placeholder="Nhà Đất..." required> </div>
                            </div>
                            <!--/span-->
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <!--/row-->
                        <div class="row">
                            <!--/span-->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Mô tả</label>
                                    <textarea name="description" class="form-control" rows="5" placeholder="Nhập mô tả"></textarea>
                                    <br/>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                    </div>
                    <div class="form-actions m-t-40">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Lưu</button>
                        <button type="button" class="btn btn-dark">Huỷ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
@stop
