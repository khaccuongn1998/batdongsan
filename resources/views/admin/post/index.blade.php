@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Bài Viết','key'=>'Xem'])
@stop
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <!-- title -->
                    <div class="d-md-flex align-items-center">
                        <div>
                            <h4 class="card-title">Bài viết</h4>
                        </div>
                        <div class="ml-auto">
                            <div class="dl">
                                <a href="{{ route('admin.post.add') }}">
                                    <button data-repeater-create="" class="btn btn-info waves-effect waves-light">Thêm Bài Viết
                                    </button>
                                </a>
                                <select class="custom-select">
                                    <option value="0" selected="">Monthly</option>
                                    <option value="1">Daily</option>
                                    <option value="2">Weekly</option>
                                    <option value="3">Yearly</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- title -->
                </div>
                <div class="table-responsive">
                    <table class="table v-middle">
                        <thead>
                        <tr class="bg-light">
                            <th class="border-top-0">STT</th>
                            <th class="border-top-0" width="20%">Tên danh mục</th>
                            <th class="border-top-0">Hình ảnh</th>
                            <th class="border-top-0">Ngày Tạo</th>
                            <th class="border-top-0">Lượt xem</th>
                            <th class="border-top-0">Danh mục bài viết</th>
                            <th class="border-top-0" width="200px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $result as $item)
                        <tr>
                            <td>1</td>
                            <td>{{ $item->name }}</td>
                            <td>
                                <img src="{{ $item->image }}" alt="" width="100px">
                            </td>
                            <td>
                                {{ $item->created_at->format('d/m/Y') }}
                            </td>
                            <td>
                                {{ $item->is_viewed }}
                            </td>

                            <td>
                                {{ getNamePostCategoryById($item->post_category_id) }}
                            </td>
                            <td>
                                <a href="{{ route('admin.post.edit',[$item->id]) }}">
                                    <button data-repeater-edit="" class="btn btn-info waves-effect waves-light">Sửa
                                    </button>
                                </a>
                                <button data-repeater-delete=""
                                        data-url="{{ route('admin.post.delete',[$item->id]) }}"
                                        class="btn btn-danger waves-effect waves-light m-l-10 delete-confirm"
                                        type="button">
                                    Xoá
                                </button>
                            </td>
                        </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@stop
