@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Danh mục Bài viết','key'=>'Thêm'])
@stop
@section('content')

    <div class="row">
        <!-- Column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.post.create') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <h5 class="card-title">Bài Viết</h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Tên Bài Viết</label>
                                        <input type="text" name="name" id="firstName" class="form-control" placeholder="Nhập tên bài viết"></div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <h5 class="card-title m-t-20">Upload Hình Ảnh</h5>
                                    <div class="btn btn-info waves-effect waves-light"><span>Upload Hình Ảnh khác</span>
                                        <input type="file" name="image" class="upload"></div>
                                </div>
                            </div>
                            <hr>
                            <!--/row-->
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Danh mục bài viết</label>
                                        <select class="form-control" name="post_category_id" data-placeholder="Chọn Danh mục" tabindex="1">
                                            @foreach( getAllPostCategory() as $cate)
                                                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h5 class="card-title m-t-40">Mô tả</h5>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <textarea class="form-control" name="description" rows="4" placeholder="Mô tả"></textarea>
                                    </div>
                                </div>
                            </div>

                            <h5 class="card-title m-t-40">Nội dung bài viết</h5>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <textarea class="form-control" name="content" id="content-editor" rows="10" placeholder="Nội dung bài
                                        viết"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-actions m-t-40">
                            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save</button>
                            <button type="button" class="btn btn-dark">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
@stop
