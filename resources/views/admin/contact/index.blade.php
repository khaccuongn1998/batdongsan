@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Liên hệ','key'=>'Xem'])
@stop
@section('content')

    <div class="email-app border-top">
        <!-- ============================================================== -->
        <!-- Left Part -->
        <!-- ============================================================== -->
        <div class="left-part bg-light">
            <a class="ti-menu ti-close btn btn-success show-left-part d-block d-md-none" href="javascript:void(0)"></a>
            <div class="scrollable" style="height:100%;">
                <div class="p-15">
                    <a id="compose_mail" class="waves-effect waves-light btn btn-danger d-block" href="javascript: void(0)">Chính</a>
                </div>
                <div class="divider"></div>
                <ul class="list-group">
                    <li>
                        <small class="p-15 grey-text text-lighten-1 db">Thư mục</small>
                    </li>
                    <li class="list-group-item">
                        <a href="javascript:void(0)" class="active list-group-item-action"><i class="mdi mdi-inbox"></i> Inbox <span
                                class="label label-success float-right">{{ $resultAll->count() }}</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Right Part -->
        <!-- ============================================================== -->
        <div class="right-part mail-list bg-white">
            <div class="p-15 b-b">
                <div class="d-flex align-items-center">
                    <div>
                        <h4>Liên hệ</h4>
                        <span>Tất cả liên hệ</span>
                    </div>
                    <div class="ml-auto">
                        <input placeholder="Search Mail" id="" type="text" class="form-control">
                    </div>
                </div>
            </div>
            <!-- Action part -->
            <div class="table-responsive">
                <table class="table email-table no-wrap table-hover v-middle">
                    <tbody>
                    <!-- row -->
                    @foreach($resultAll as $result)
                        <tr class="{{ $result->is_read===0 ? 'unread' : ''}}">
                            <!-- label -->
                            <!-- star -->
                            <td class="starred"><i class="far fa-star"></i></td>
                            <!-- User -->
                            <td class="user-image"><i class="fas fa-address-book"></i></td>
                            <td class="user-name">
                                <h6 class="m-b-0">{{ $result->name }}</h6>
                            </td>
                            <!-- Message -->
                            <td class="max-texts">
                                <a class="link" href="{{ route('admin.contact.detail',[$result->id]) }}">
                                    @if($result->is_read===0)
                                        <span class="label label-danger m-r-10">Chưa xem</span>
                                    @else
                                        <span class="label label-success m-r-10">Đã xem</span>
                                    @endif
                                    <span class="blue-grey-text text-darken-4">{{ $result->content }}</a></td>
                            <!-- Attachment -->
                            <td class="clip"><i class="fa fa-paperclip"></i></td>
                            <!-- Time -->
                            <td class="time">{{ $result->created_at->format('d/m/Y') }}</td>
                        </tr>
                    @endforeach
                    <!-- row -->
                    </tbody>
                </table>
            </div>
            <div class="p-15 m-t-30">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item"><a class="page-link" href="javascript:void(0)">Previous</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0)">1</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                        <li class="page-item"><a class="page-link" href="javascript:void(0)">Next</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Right Part  Mail Compose -->
        <!-- ============================================================== -->

    </div>

@stop
