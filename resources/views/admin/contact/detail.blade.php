@extends('admin.layout.master')
@section('breadcrumbs')
    @include('admin.path.bread-crumb',['name'=>'Liên hệ','key'=>'Chi Tiết'])
@stop
@section('content')
    <div class="email-app border-top">
        <!-- ============================================================== -->
        <!-- Left Part -->
        <!-- ============================================================== -->
        <div class="left-part bg-light">
            <a class="ti-menu ti-close btn btn-success show-left-part d-block d-md-none" href="javascript:void(0)"></a>
            <div class="scrollable" style="height:100%;">
                <div class="p-15">
                    <a id="compose_mail" class="waves-effect waves-light btn btn-danger d-block" href="javascript: void(0)">Chính</a>
                </div>
                <div class="divider"></div>
                <ul class="list-group">
                    <li>
                        <small class="p-15 grey-text text-lighten-1 db">Thư Mục</small>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ route('admin.contact.index') }}" class="active list-group-item-action">
                            <i class="mdi mdi-inbox"></i> Inbox
                            <span class="label label-success float-right">{{ getAllContact()->count() }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Right Part  Mail detail -->
        <!-- ============================================================== -->
        <div class="right-part mail-details bg-white">
            <div class="card-body border-bottom">
                <h4 class="m-b-0">Tên : {{ $result->name ?? 'Vô danh' }}</h4>
            </div>
            <div class="card-body border-bottom">
                <div class="d-flex no-block align-items-center m-b-40">
                    <div class="">
                        <h5 class="m-b-0 font-16 font-medium">Số điện thoại : {{ $result->phone }}
                        </h5>
                        <span>Email : {{ $result->email }}</span> <br/>
                        <span>Ngày gửi : {{ $result->created_at->format('d/m/Y H:i:s') }}</span>
                    </div>
                </div>
                <h4 class="m-b-15">Nội dung</h4>
                <p>{{ $result->content }}</p>
                 <button id="delete-confirm"
                         data-url = {{ route('admin.contact.delete',[$result->id]) }}
                         type="button" class="btn
                btn-danger">Xoá</button>
            </div>

        </div>
    </div>
@stop
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#delete-confirm').click(function (e) {
                e.preventDefault();
                var url = $(this).data('url');
                var that = $(this);
                Swal.fire({
                    title: 'Bạn chắc chắn muốn xóa?',
                    text: "Dữ liệu sẽ bị xoá hoàn toàn!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Có, Tôi muốn xóa!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "get",
                            url: url,
                            success: function (response) {
                                that.parent().parent().remove();
                                Swal.fire(
                                    'Xóa Thành công!',
                                    'Dữ liệu đã được xóa.',
                                    'success'
                                );
                                location.href = "{{ route('admin.contact.index') }}"
                            },
                            error: function (response) {
                            }
                        });
                    }
                })
            });
        });
    </script>
@endpush
