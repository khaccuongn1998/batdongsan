<!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="">

    <!-- CSS
	============================================ -->

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('web/assets/css/plugins.css') }}">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{ asset('web/assets/css/style.css') }}">


