
<!-- Map js code here -->
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3nDHy1dARR-Pa_2jjPCjvsOR4bcILYsM"></script>
<!-- Plugins JS -->
<script src="{{ asset('web/assets/js/plugins.js') }}"></script>
<!-- Map Active JS -->
<script src="{{ asset('web/assets/js/maplace-active.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('web/assets/js/main.js') }}"></script>
@stack('styles')
