<header class="header-wrapper section">
    <div class="header-top bg-theme-two section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="header-top-info">
                        <p class="text-white">Call us -  <a href="tel:21548987658">21548 987 658</a></p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="header-buttons">
                        <a class="header-btn btn" href="add-property.html">Add Property</a>
                        <a class="header-btn btn-border" href="register.html" >Register</a>
                        <a class="header-btn" href="login.html">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="header-section section">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-2 col-6">
                    <div class="header-logo">
                        <a href="index.html"><img src="{{ asset('web/assets/images/logo.png') }}" alt=""></a>
                    </div>
                </div>

                <div class="col-lg-10 col-6">
                    <div class="header-mid_right-bar">
                       @include('web.menu.home')
                        <div id="search-overlay-trigger" class="search-icon">
                            <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                        </div>
                    </div>
                </div>

                <!-- Mobile Menu -->
                <div class="mobile-menu order-12 d-block d-lg-none col"></div>

            </div>
        </div>
    </div><!-- Header Section End -->
</header>

<!--  search overlay -->
<div class="search-overlay" id="search-overlay">

    <div class="search-overlay__header">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-6 ml-auto col-4">
                    <!-- search content -->
                    <div class="search-content text-right">
                        <span class="mobile-navigation-close-icon" id="search-close-trigger"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="search-overlay__inner">
        <div class="search-overlay__body">
            <div class="search-overlay__form">
                <form action="#">
                    <input type="text" placeholder="Search">
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End of search overlay -->
