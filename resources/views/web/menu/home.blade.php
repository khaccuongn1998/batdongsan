<nav class="main-menu d-lg-block d-none">
    <ul>
        <li><a href="{{ route('index') }}">Trang chủ</a></li>
        <li><a href="features.html">Giới thiệu</a></li>
        <li class="has-dropdown"><a href="{{ route('project.index') }}">Dự án</a>
            <ul class="sub-menu">
                <li><a href="{{ route('project.index') }}">Dự án</a></li>
                <li><a href="properties-left-sidebar.html">Properties Left Sidebar</a></li>
                <li><a href="properties-right-sidebar.html">Properties Right Sidebar</a></li>
                <li><a href="properties-details.html">Properties Details</a></li>
                <li><a href="add-property.html">Add Propertie</a></li>
            </ul>
        </li>
        <li class="has-dropdown"><a href="{{ route('post.index') }}">Tin tức</a>
            <ul class="sub-menu">
                <li><a href="about-us.html">About Page</a></li>
                <li><a href="create-agency.html">Create agency</a></li>
                <li><a href="login.html">Login Page</a></li>
                <li><a href="register.html">Register Page</a></li>
            </ul>
        </li>
        <li><a href="{{ route('contact.index') }}">Liên hệ</a></li>
    </ul>
</nav>
