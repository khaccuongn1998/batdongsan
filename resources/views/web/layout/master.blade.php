<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @yield('title')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @include('web.path.head')
    {!! NoCaptcha::renderJs() !!}
</head>
<body>


<!-- Header Section Start -->
@include('web.path.header')

<!-- Hero Section Start -->
@yield('breadcrumb')
@yield('content')
@include('web.path.footer')
<!-- JS
============================================ -->

@include('web.path.script')
@stack('script')

</body>

</html>
