<div class="col-lg-4 col-xl-3 col-12 order-lg-1 order-2">
    <div class="row widgets">

        <div class="col-lg-12">
            <div class="single-widget widget-search">
                <h4 class="widget-title">
                    <span>Find your home</span>
                </h4>
                <div class="search-wrap sidebar-wigets-search">
                    <form action="#">
                        <div class="row row-10">

                            <div class="col-lg-6 col-md-6 col-12 mb-20">
                                <select class="nice-select">
                                    <option>For Rent</option>
                                    <option>For Sale</option>
                                </select>
                            </div>

                            <div class="col-lg-6 col-md-6 col-12 mb-20">
                                <select class="nice-select">
                                    <option>Location </option>
                                    <option>Location 2</option>
                                    <option>Location 3</option>
                                    <option>Location 4</option>
                                </select>
                            </div>

                            <div class="col-lg-6 col-md-6 col-12 mb-20">
                                <select class="nice-select">
                                    <option>All Types</option>
                                    <option>Types One</option>
                                    <option>Types Two</option>
                                    <option>Types Three</option>
                                </select>
                            </div>

                            <div class="col-lg-6 col-md-6 col-12 mb-20">
                                <select class="nice-select">
                                    <option>Area(sqft)</option>
                                    <option>800(sqft)</option>
                                    <option>1200(sqft)</option>
                                    <option>1600(sqft)</option>
                                </select>
                            </div>

                            <div class="col-lg-6 col-md-6 col-12 mb-20">
                                <select class="nice-select">
                                    <option>Bedroom</option>
                                    <option>0 Bedroom</option>
                                    <option>1 Bedroom</option>
                                    <option>2 Bedroom</option>
                                    <option>3 Bedroom</option>
                                    <option>4 Bedroom</option>
                                </select>
                            </div>

                            <div class="col-lg-6 col-md-6 col-12 mb-20">
                                <select class="nice-select">
                                    <option>Bathroom</option>
                                    <option>0 Bathroom</option>
                                    <option>1 Bathroom</option>
                                    <option>2 Bathroom</option>
                                    <option>3 Bathroom</option>
                                    <option>4 Bathroom</option>
                                </select>
                            </div>

                            <div class="col-lg-8 ml-auto mr-auto col-12 mb-20">
                                <div id="price-range"></div>
                            </div>

                            <div class="col-lg-12 col-md-6 col-12 mb-30">
                                <div class="serche-input-box  ml-auto mr-auto">
                                    <input type="submit" value="search">
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="single-widget widget">
                <h4 class="widget-title">
                    <span>New Added Property</span>
                </h4>
                <div class="row single-propertiy-wigets">
                    <div class="col-lg-12 col-md-6 single-propertiy mb-30">
                        <a href="#"><img src="assets/images/propertes/w-propertie-01.jpg" alt=""></a>
                        <div class="propertiy-det-box">
                            <h4><a href="#">Casel la Denver</a></h4>
                            <p>Price $1,09,000</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-6 single-propertiy mb-30">
                        <a href="#"><img src="assets/images/propertes/w-propertie-03.jpg" alt=""></a>
                        <div class="propertiy-det-box">
                            <h4><a href="#">White Smith Casel</a></h4>
                            <p>Price $1,03,000</p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-6 single-propertiy mb-30">
                        <a href="#"><img src="assets/images/propertes/w-propertie-02.jpg" alt=""></a>
                        <div class="propertiy-det-box">
                            <h4><a href="#">Casel la Denver</a></h4>
                            <p>Price $1,08,000</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="single-widget widget">

                <h4 class="widget-title">
                    <span>Our Agent</span>
                </h4>

                <div class="our-widget-agent">
                    <div class="widget-agent">
                        <div class="image">
                            <a href="#"><img src="assets/images/agents/side-01.jpg" alt=""></a>
                        </div>
                        <div class="name">
                            <h5>Bayazid Smith</h5>
                        </div>
                    </div>
                    <div class="widget-agent">
                        <div class="image">
                            <a href="#"> <img src="assets/images/agents/side-02.jpg" alt=""></a>
                        </div>
                        <div class="name">
                            <h5>Bayazid Smith</h5>
                        </div>
                    </div>
                    <div class="widget-agent">
                        <div class="image">
                            <a href="#"><img src="assets/images/agents/side-03.jpg" alt=""></a>
                        </div>
                        <div class="name">
                            <h5>Bayazid Smith</h5>
                        </div>
                    </div>
                    <div class="widget-agent">
                        <div class="image">
                            <a href="#"><img src="assets/images/agents/side-04.jpg" alt=""></a>
                        </div>
                        <div class="name">
                            <h5>Bayazid Smith</h5>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-12">
            <div class="single-widget widget-tag">
                <h4 class="widget-title">
                    <span>Tag</span>
                </h4>
                <div class="tag">
                    <a href="#">Real Estate</a>
                    <a href="#">Home</a>
                    <a href="#">Duplex</a>
                    <a href="#">Villa</a>
                    <a href="#">Appartment</a>
                    <a href="#">Property</a>
                </div>
            </div>
        </div>

    </div>
</div>
