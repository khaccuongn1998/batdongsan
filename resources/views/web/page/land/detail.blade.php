@extends('web.layout.master')
@section('title')
@endsection

@section('breadcrumb')
    @include('web.path.breadcrumb',['banner'=>getSlider('about-banner')->first()->image])
@endsection

@section('content')
    <main class="page-content section">

        <!-- Featured Properites Start -->
        <div class="properites-sidebar-wrap pt-80 pt-md-60 pt-sm-40 pt-xs-30 pb-110 pb-md-90 pb-sm-70 pb-xs-60">
            <div class="container">

                <div class="row">
                   @include('web.page.project.right.right')
                    <div class="col-lg-8 col-xl-9 col-12 order-lg-1 order-1">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="blog-details-warpper">
                                    <div class="details-image mt-30">
                                        <img src="{{ $result->image }}" alt="" width="870px" height="465px">
                                    </div>
                                    <div class="details-contents-wrap">

                                        <h3>{{ $result->name }}</h3>

                                        <p class="mt-20">{!! $result->content !!}</p>


                                        <div class="propertice-details pt-25">
                                            <div class="row">

                                                <div class="col-12">
                                                    <div class="properties-details-title mb-10">
                                                        <h4>Trạng thái</h4>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <strong>Diện tích</strong><span>{{ $result->area }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <div class="single-info">
                                                        <strong>Vị trí:</strong><span>{{ $result->location }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <strong>Khu vực:</strong><span>{{ $result->address }}</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-property-price">
                                                        <strong>Giá: {{ $result->price }}</strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="propertice-details pt-25">
                                            <div class="row">

                                                <div class="col-12">
                                                    <div class="properties-details-title mb-10">
                                                        <h4>Amenities</h4>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Air Conditioning</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <div class="single-info">
                                                        <span>Bedding</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Balcony</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Cable TV</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Internet</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Parking</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>lift</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Home Theater</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Pool</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Dishwasher</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="single-info">
                                                        <span>Toaster</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="propertice-details pt-25">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="properties-details-title mb-20">
                                                                <h4>Floor Plan</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="image">
                                                                <img src="{{ asset('web/assets/images/propertes/property-map.jpg') }}" alt="">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="comments-area pt-70 pt-md-50 pt-sm-50 pt-xs-50">
                                        <h4>Comments</h4>

                                        <ul class="comment-list">
                                            <li>
                                                <div class="comment">
                                                    <div class="image"><img src="assets/images/review/01.png" alt=""></div>
                                                    <div class="content">
                                                        <h5>Luci Chunni</h5>
                                                        <div class="d-flex flex-wrap justify-content-between">
                                                            <span class="time">6 hour ago</span>
                                                        </div>
                                                        <div class="decs">
                                                            <p>There are some business lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                                eiu tepunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                                nostrudt </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="child-comment">
                                                    <li>
                                                        <div class="comment">
                                                            <div class="image"><img src="assets/images/review/02.png" alt=""></div>
                                                            <div class="content">
                                                                <h5>Devid Bepari</h5>
                                                                <div class="d-flex flex-wrap justify-content-between">
                                                                    <span class="time">10 hour ago</span>
                                                                </div>
                                                                <div class="decs">
                                                                    <p>There are some business lorem ipsum dolor sit amet, consectetur adipiscing
                                                                        elit, sed do eiu tempunt ut labore et dolore magna aliqua. Ut enim ad minim
                                                                        veniam</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="comment">
                                                    <div class="image"><img src="assets/images/review/03.png" alt=""></div>
                                                    <div class="content">
                                                        <h5>Neha Jhograti</h5>
                                                        <div class="d-flex flex-wrap justify-content-between">
                                                            <span class="time">6 hour ago</span>
                                                        </div>
                                                        <div class="decs">
                                                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and ising
                                                                pain borand I will give you a complete account of the system</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                        <h4>Leave a Comments</h4>

                                        <div class="comment-form">
                                            <form action="#">
                                                <div class="row">
                                                    <div class="col-md-6 col-12 mb-30"><input type="text" placeholder="Your Name"></div>
                                                    <div class="col-md-6 col-12 mb-30"><input type="email" placeholder="Email"></div>
                                                    <div class="col-md-6 col-12 mb-30"><input type="text" placeholder="Phone"></div>
                                                    <div class="col-md-6 col-12 mb-30"><input type="text" placeholder="Subject"></div>
                                                    <div class="col-12 mb-30"><textarea placeholder="Message"></textarea></div>
                                                    <div class="col-12">
                                                        <button class="btn send-btn btn-circle">Send</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div><!-- Featured Properites End -->

    </main>
@endsection
