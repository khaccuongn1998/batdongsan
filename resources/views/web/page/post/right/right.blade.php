<div class="col-lg-4  order-lg-1 order-2">
    <div class="row widgets left-sidebar">

        <div class="col-lg-12">
            <div class="single-widget widget-search mb-25">
                <h4 class="widget-title">
                    <span>Tìm kiếm</span>
                </h4>
                <form action="#" class="widget-search-form">
                    <input type="text" placeholder="Search...">
                </form>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="single-widget widget-categories mb-25">
                <h4 class="widget-title">
                    <span>Danh mục</span>
                </h4>
                <ul>
                    @foreach(getAllPostCategory() as $postCategory)
                        <li><a href="#" class="categories-name">{{ $postCategory->name }}<span>{{
                                            $postCategory->posts->count()
                                            }}</span></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="single-widget widget-related mb-25">
                <h4 class="widget-title">
                    <span>Tin tức mới nhất</span>
                </h4>
                <ul>
                    @foreach(getLastBlogLimit() as $blog)
                        <li class="single-related">
                            <div class="single-related-image">
                                <a href="#"><img src="{{ $blog->image }}" width="113px" height="81px" alt=""></a>
                            </div>
                            <div class="single-related-contents">

                                <h4><a href="single-blog.html">{{ $blog->name }}</a></h4>
                                <div class="post_meta">
                                    <ul>
                                        <li><p>{{ $blog->created_at->format('d/m/Y') }}</p></li>
                                    </ul>
                                </div>
                                <p>{{ $blog->description }}</p>
                            </div>
                        </li>
                    @endforeach
                </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="single-widget widget-newsletter">
                <h4 class="widget-title">
                    <span>Tag</span>
                </h4>
                <div class="tag">
                    <a href="#">Real Estate</a>
                    <a href="#">Home</a>
                    <a href="#">Appartment</a>
                    <a href="#">Duplex villa</a>
                    <a href="#">Property</a>
                    <a href="#">Sale</a>
                    <a href="#">Broker</a>
                    <a href="#">Agent</a>
                    <a href="#">Agency</a>
                </div>
            </div>
        </div>
    </div>

</div>
