@extends('web.layout.master')



@section('breadcrumb')
    @include('web.path.breadcrumb',['banner'=>getSlider('about-banner')->first()->image])
@endsection

@section('content')

    <main class="page-content section">
        <!-- Blog Grids Area -->
        <div class="blog-grids-area pt-80 pt-md-60 pt-sm-40 pt-xs-30 pb-110 pb-md-90 pb-sm-70 pb-xs-60">
            <div class="container">

                <div class="row">
                    <div class="col-lg-8 order-lg-2 order-1">
                        <div class="row">
                            <div class="col-12">
                                <!-- blog-wrapper start -->
                                <div class="blog-wrapper">

                                    <div class="row">
                                        @foreach($result as $blog)
                                            <div class="col-lg-6 col-md-6 mt-30">
                                                <!-- Single latest blog Start -->
                                                <div class="single-latest-blog">
                                                    <div class="latest-blog-image">
                                                        <a href="{{ route('post.detail',[$blog->slug]) }}">
                                                            <img src="{{ $blog->image }}" alt=""></a>
                                                    </div>
                                                    <div class="latest-blog-contents">
                                                        <h4><a href="{{ route('post.detail',[$blog->slug]) }}">{{ $blog->name }}</a></h4>
                                                        <p><span>{{ $blog->created_at->format('d/m/Y') }}</span> / <span>{{
                                                        $blog->created_at->format('H:i') }}</span></p>
                                                        <p>{{ $blog->description }} </p>
                                                        <a class="read-more" href="{{ route('post.detail',[$blog->slug]) }}">Đọc tiếp</a>
                                                    </div>
                                                </div><!-- Single latest blog End -->
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- paginatoin-area Start -->
                        <div class="row pt-40">
                            <div class="col">
                                <ul class="page-pagination">
                                    @if($result->lastPage() >1)
                                        <li><a href="{{ $result->url($result->currentPage()-1)}}"
                                               class="{{ ($result->currentPage() === 1) ? 'disabled' : '' }}"
                                            >
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                        </li>
                                        @for($i = 1 ; $i<= $result->lastPage() ; $i++)
                                            <li class={{ ($result->currentPage() === $i) ? 'active' : '' }}
                                            ><a href="{{ $result->url($i) }}">{{ $i }}</a></li>
                                        @endfor
                                        <li><a href="{{ $result->url($result->currentPage()+1) }}"
                                               class="{{ $result->currentPage() == $result->lastPage() ? 'disabled' : ''}}"
                                            ><i class="fa fa-angle-right"></i></a></li>
                                    @endif
                                </ul>
                            </div>
                        </div><!--// paginatoin-area End -->

                    </div>

                    @include('web.page.post.right.right')
                </div>
            </div>
        </div>
        <!--// Blog Grids Area -->
    </main>

@endsection
