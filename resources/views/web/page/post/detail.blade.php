@extends('web.layout.master')
@section('breadcrumb')
    @include('web.path.breadcrumb',['banner'=>getSlider('about-banner')->first()->image])
@endsection

@section('content')
    <main class="page-content section">
        <!-- Blog Grids Area -->
        <div class="blog-grids-area pt-80 pt-md-60 pt-sm-40 pt-xs-30 pb-110 pb-md-90 pb-sm-70 pb-xs-60">
            <div class="container">

                <div class="row">
                    <div class="col-lg-8 order-lg-1 order-1">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="blog-details-warpper">
                                    <div class="details-image mt-30">
                                        <img src="{{ $result->image }}" width="770px" height="450px" alt="">
                                    </div>
                                    <div class="details-contents-wrap">

                                        <h3>{{ $result->name }}</h3>
                                        <p><span>{{ $result->created_at->format('d/m/Y') }}</span> / <span>{{ $result->created_at->format('H:i')
                                        }}</span></p>

                                        <div>
                                            {!! $result->content !!}
                                        </div>

                                        <div class="blog-marketing-wrap">
                                            <div class="tags">
                                                <strong>Tags:</strong>  <a href="#">Apartment</a>, <a href="#">Building</a>, <a href="#">Real Estate</a>, <a href="#">Commercial</a>
                                            </div>
                                            <div class="share-socail">
                                                <strong>Share: </strong>
                                                <ul>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="comments-area pt-70 pt-md-50 pt-sm-50 pt-xs-50">
                                        <h4>Comments</h4>

                                        <ul class="comment-list">
                                            <li>
                                                <div class="comment">
                                                    <div class="image"><img src="assets/images/review/01.png" alt=""></div>
                                                    <div class="content">
                                                        <h5>Luci Chunni</h5>
                                                        <div class="d-flex flex-wrap justify-content-between">
                                                            <span class="time">6 hour ago</span>
                                                        </div>
                                                        <div class="decs">
                                                            <p>There are some business lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu tepunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudt </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="child-comment">
                                                    <li>
                                                        <div class="comment">
                                                            <div class="image"><img src="assets/images/review/02.png" alt=""></div>
                                                            <div class="content">
                                                                <h5>Devid Bepari</h5>
                                                                <div class="d-flex flex-wrap justify-content-between">
                                                                    <span class="time">10 hour ago</span>
                                                                </div>
                                                                <div class="decs">
                                                                    <p>There are some business lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu tempunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="comment">
                                                    <div class="image"><img src="assets/images/review/03.png" alt=""></div>
                                                    <div class="content">
                                                        <h5>Neha Jhograti</h5>
                                                        <div class="d-flex flex-wrap justify-content-between">
                                                            <span class="time">6 hour ago</span>
                                                        </div>
                                                        <div class="decs">
                                                            <p>But I must explain to you how all this mistaken idea of denouncing pleasure and ising pain  borand I will give you a complete account of the system</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                        <h4>Leave a Comments</h4>

                                        <div class="comment-form">
                                            <form action="#">
                                                <div class="row">
                                                    <div class="col-md-6 col-12 mb-30"><input type="text" placeholder="Your Name"></div>
                                                    <div class="col-md-6 col-12 mb-30"><input type="email" placeholder="Email"></div>
                                                    <div class="col-md-6 col-12 mb-30"><input type="text" placeholder="Phone"></div>
                                                    <div class="col-md-6 col-12 mb-30"><input type="text" placeholder="Subject"></div>
                                                    <div class="col-12 mb-30"><textarea placeholder="Message"></textarea></div>
                                                    <div class="col-12"><button class="btn send-btn btn-circle">Send</button></div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    @include('web.page.post.right.right')
                </div>
            </div>
        </div>
        <!--// Blog Grids Area -->
    </main>
@endsection
