@extends('web.layout.master')
@section('title')
@endsection
@section('breadcrumb')
    @include('web.path.breadcrumb',['banner'=>getSlider('about-banner')->first()->image])
@endsection
@section('content')
    <!-- Our Agents Section Start -->
    <div class="contact-section section pt-110 pt-md-90 pt-sm-70 pt-xs-60 pb-110 pb-md-90 pb-sm-70 pb-xs-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-us-wrap">
                        <div class="contact-title pb-30">
                            <h4>Liên Hệ Với <span>Chúng Tôi</span></h4>
                            <p>abc</p>
                        </div>

                        <div class="contact-info">
                            <ul>
                                <li>
                                    <div class="contact-text d-flex align-items-center">
                                        <i class="glyph-icon flaticon-placeholder"></i>
                                        <p>256, 1st AVE, Manchester <br>125 , Noth England</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="contact-text d-flex align-items-center">
                                        <i class="glyph-icon flaticon-call"></i>
                                        <p>
                                            <span>Telephone : <a href="#"> +88 (012) 356 958 45</a></span>
                                            <span>Telephone : <a href="#"> +88 (012) 356 958 45</a></span>
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="contact-text d-flex align-items-center">
                                        <i class="glyph-icon flaticon-earth"></i>
                                        <p>
                                            <span>Email : <a href="#">info@example.com</a></span>
                                            <span>Web : <a href="#">www.example.com</a></span>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact-us-wrap">
                        <h4>Leave a Message</h4>

                        <div class="contact-form">
                            <form id="contact-form" action="{{ route('contact.create') }} ">
                                @csrf
                                <div class="row row-10">
                                    <div class="col-md-6 col-12 mb-30"><input  name="name" type="text" placeholder="Tên" required></div>
                                    <div class="col-md-6 col-12 mb-30"><input  name="phone" type="text" placeholder="Số điện thoại" required></div>
                                    <div class="col-md-6 col-12 mb-30"><input  name="email" type="email" placeholder="Email" required></div>
                                    <div class="col-12 mb-30"><textarea name="content"  placeholder="Nội dung"></textarea></div>
                                    <div class="col-12 mb-30">{!! app('captcha')->display() !!}</div>
                                    <div class="col-12"><button class="btn send-btn btn-circle">Send</button></div>
                                </div>
                            </form>
                            <p class="form-messege"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- Our Agents Section End -->

    <div class="embed-responsive embed-responsive-21by9">
        <div id="contact-map" class="embed-responsive-item contact-map" data-lat="40.828411" data-Long="-74.589912"></div>
    </div>
@endsection

