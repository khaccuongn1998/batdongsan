@extends('web.layout.master')
@section('title')
@endsection

@section('breadcrumb')
    @include('web.path.breadcrumb',['banner'=>getSlider('about-banner')->first()->image])
@endsection

@section('content')

    <main class="page-content section">

        <!-- Featured Properites Start -->
        <div class="properites-sidebar-wrap pt-110 pt-md-90 pt-sm-70 pt-xs-60 pb-110 pb-md-90 pb-sm-70 pb-xs-60
       ">
            <div class="container">

                <div class="row">
                    <div class="col">
                        <ul class="properties-list nav justify-content-end" role="tablist">
                            <li class="active"><a class="active" href="#all" role="tab" data-toggle="tab">ALL</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    @include('web.page.project.right.right')
                    <div class="col-lg-8 col-xl-9 col-12 order-lg-2 order-1">

                        <div class="tab-content">
                            <div class="tab-pane active" id="all">
                                <div class="row">
                                    @foreach( $result as $project)
                                        <div class="col-xl-4 col-lg-6 col-md-4 col-12">
                                            <!-- single-property Start -->
                                            <div class="single-property mt-30">
                                                <div class="property-img">
                                                    <a href="{{ route('project.detail',[$project->slug]) }}">
                                                        <img src="{{ $project->image }}" alt="">
                                                    </a>
                                                </div>
                                                <div class="property-desc">
                                                    <h4><a href="{{ route('project.detail',[$project->slug]) }}">{{ $project->name }}</a></h4>
                                                    <p>
                                                        <p class="location">{{ $project->location }}</p>
                                                        <span class="property-info">{{ $project->address }}</span>
                                                    </p>
                                                    <div class="price-box">
                                                        <p>Giá: {{ $project->price }}</p>
                                                    </div>
                                                </div>
                                            </div><!-- single-property End -->
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="row pt-40">
                            <div class="col">
                                <ul class="page-pagination">
                                    @if($result->lastPage() >1)
                                        <li><a href="{{ $result->url($result->currentPage()-1)}}"
                                            class="{{ ($result->currentPage() === 1) ? 'disabled' : '' }}"
                                            >
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                        </li>
                                        @for($i = 1 ; $i<= $result->lastPage() ; $i++)
                                            <li class={{ ($result->currentPage() === $i) ? 'active' : '' }}
                                            ><a href="{{ $result->url($i) }}">{{ $i }}</a></li>
                                        @endfor
                                        <li><a href="{{ $result->url($result->currentPage()+1) }}"
                                               class="{{ $result->currentPage() == $result->lastPage() ? 'disabled' : ''}}"
                                            ><i class="fa fa-angle-right"></i></a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div><!-- Featured Properites End -->

    </main>

@endsection
