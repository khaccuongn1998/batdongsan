$(document).ready(function () {
    $('.delete-confirm').click(function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        var that = $(this);
        Swal.fire({
            title: 'Bạn chắc chắn muốn xóa?',
            text: "Dữ liệu sẽ bị xoá hoàn toàn!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Có, Tôi muốn xóa!'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        that.parent().parent().remove();
                        Swal.fire(
                            'Xóa Thành công!',
                            'Dữ liệu đã được xóa.',
                            'success'
                        )
                    },
                    error: function (response) {
                    }
                });
            }
        })
    });
});
