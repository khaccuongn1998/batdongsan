<?php
return [
    [
        'name' => 'Dashboard',
        'route' => 'admin.home',
        'icon' => 'icon-Car-Wheel',
        'items' => [],

    ],
    [
        'name' => 'Quản lí Tin tức',
        'route' => 'javascript:void(0)',
        'icon' => 'icon-Bulleted-List',
        'items' => [
            [
                'name' => 'Danh mục tin tức',
                'route' => 'admin.category-post.index',
                'icon' => ' icon-Numbering-List',
            ],
            [
                'name' => 'Tin tức',
                'route' => 'admin.post.index',
                'icon' => ' icon-Newspaper',
            ]
        ],
    ],
    [
        'name' => 'Quản lí Dự Án',
        'route' => 'javascript:void(0)',
        'icon' => 'icon-Bulleted-List',
        'items' => [
            [
                'name' => 'Danh mục dự án',
                'route' => 'admin.project-category.index',
                'icon' => ' icon-Numbering-List',
            ],
            [
                'name' => 'Dự án',
                'route' => 'admin.project.index',
                'icon' => ' icon-Newspaper',
            ]
        ],
    ],

    [
        'name' => 'Quản lí Đất Bán',
        'route' => 'javascript:void(0)',
        'icon' => 'icon-Bulleted-List',
        'items' => [
            [
                'name' => 'Danh mục đất bán',
                'route' => 'admin.land-category.index',
                'icon' => ' icon-Numbering-List',
            ],
            [
                'name' => 'Đất bán',
                'route' => 'admin.land.index',
                'icon' => ' icon-Newspaper',
            ]
        ],
    ],

    [
        'name' => 'Quản lí Slider',
        'route' => 'javascript:void(0)',
        'icon' => 'icon-Bulleted-List',
        'items' => [
            [
                'name' => 'Danh mục slider',
                'route' => 'admin.slider.index',
                'icon' => ' icon-Numbering-List',
            ],
            [
                'name' => 'Slider',
                'route' => 'admin.slider-item.index',
                'icon' => ' icon-Newspaper',
            ]
        ],
    ],

    [
        'name' => 'Menu',
        'route' => 'admin.menu.index',
        'icon' => 'icon-Car-Wheel',
        'items' => [],

    ],

    [
        'name' => 'Liên hệ',
        'route' => 'admin.contact.index',
        'icon' => 'icon-Car-Wheel',
        'items' => [],

    ],
    [
        'name' => 'Bình Luận',
        'route' => 'admin.comment.index',
        'icon' => 'icon-Car-Wheel',
        'items' => [],

    ],


];
