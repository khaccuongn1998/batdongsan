<?php
namespace App\Repositories\Contact;


use App\Models\Contact;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;

class ContactEloquentRepository extends EloquentRepository{
    /**
     * Get Model
     * @return string
     */

    public function getModel(){
        return Contact::class;
    }

}
