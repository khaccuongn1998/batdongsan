<?php
 namespace App\Repositories\Post;


 use App\Models\Post;
 use App\Repositories\EloquentRepository;
 use Illuminate\Support\Carbon;

 class EloquentPostRepository extends EloquentRepository implements PostRepositoryInterface{
     /**
      * Get Model
      * @return string
      */

     public function getModel(){
         return Post::class;
     }

     /**
      * Get 5 posts hot in a month the last
      * @return mixed
      */
     public function getPostHost()
     {
         return $this->_model::where('created_at', '>=', Carbon::now()->subMonth())->orderBy('id', 'desc')->take(5)->get();
     }


 }
