<?php
namespace App\Repositories\CategoryProject;


use App\Models\PostCategory;
use App\Models\ProjectCategory;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;

class CategoryProjectEloquentRepository extends EloquentRepository{
    /**
     * Get Model
     * @return string
     */

    public function getModel(){
        return ProjectCategory::class;
    }

}
