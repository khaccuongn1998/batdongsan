<?php
 namespace App\Repositories\Project;


 use App\Models\Post;
 use App\Repositories\EloquentRepository;
 use Illuminate\Support\Carbon;
 use App\Models\Project;

 class EloquentProjectRepository extends EloquentRepository{
     /**
      * Get Model
      * @return string
      */

     public function getModel(){
         return Project::class;
     }

     /**
      * Get 5 posts hot in a month the last
      * @return mixed
      */
     public function getPostHost()
     {
         return $this->_model::where('created_at', '>=', Carbon::now()->subMonth())->orderBy('id', 'desc')->take(5)->get();
     }


 }
