<?php
namespace App\Repositories\CategoryPost;


use App\Models\PostCategory;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;

class CategoryPostEloquentRepository extends EloquentRepository{
    /**
     * Get Model
     * @return string
     */

    public function getModel(){
        return PostCategory::class;
    }

}
