<?php
namespace App\Repositories\Comment;


use App\Models\Comment;
use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;

class CommentEloquentRepository extends EloquentRepository{
    /**
     * Get Model
     * @return string
     */

    public function getModel(){
        return Comment::class;
    }

}
