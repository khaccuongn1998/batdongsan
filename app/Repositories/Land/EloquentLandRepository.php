<?php


namespace App\Repositories\Land;


use App\Models\Land;
use App\Repositories\EloquentRepository;

class EloquentLandRepository extends EloquentRepository
{
    public function getModel()
    {
        // TODO: Implement getModel() method.
        return Land::class;
    }

}
