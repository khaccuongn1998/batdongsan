<?php
namespace App\Repositories;



interface RepositoryInterface{
    /**
     * Get All
     * @return mixed
     */

    public function getAll();

    /**
     * Get One
     * @param $id
     * @return mixed
     */

    public function getOne($id);

    /**
     * Create
     * @param array $data
     * @return mixed
     */

    public function create(array $data);

    /**
     * Update
     * @param $id
     * @param array $data
     * @return mixed
     */

    public function update($id,array $data);

    /**
     * Delete
     * @param $id
     * @return mixed
     */

    public function delete($id);
}
