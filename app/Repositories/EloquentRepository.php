<?php
namespace App\Repositories;


abstract class EloquentRepository implements RepositoryInterface {

    /**
     * @var \Illuminate\Database\Eloquent\Model
     * @return mixed
     */

    protected $_model;

    /**
     * Eloquent Repository Constructor
     */

    public function __construct(){
        $this->setModel();
    }

    /**
     * Set Model
     */

    public function setModel(){
        $this->_model = app()->make($this->getModel());
    }

    /**
     * Get Model
     * @return string
     */

    abstract public function getModel();

    /**
     * Get All
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */

    public function getAll(){
        return $this->_model->all();
    }

    /**
     * Get One
     * @param $id
     * @return mixed
     */

    public function getOne($id){
        return $this->_model->find($id);
    }

    /**
     * Get Paginate
     * @param int $limit
     * @return mixed
     */

    public function getPaginate(int $limit=12){
        return $this->_model->orderbyDesc('id')->paginate($limit);
    }

    /**
     * Get By Slug
     * @param $slug
     * @return mixed
     */

    public function getBySlug($slug){
        return $this->_model->where('slug', $slug)->first();
    }

    /**
     * Create
     * @param array $data
     * @return mixed
     */

    public function create(array $data){
        return $this->_model->create($data);
    }

    /**
     * Update
     * @param int $id
     * @param array $data
     * @return boolean|mixed
     */

    public function update($id, array $data){
        $result = $this->getOne($id);
        if($result){
            $result->update($data);
            return $result;
        }
        return false;
    }

    /**
     * Delete
     * @param int $id
     * @return boolean
     */

    public function delete($id){
        $result = $this->getOne($id);
        if($result){
            $result->delete();
            return true;
        }
        return false;
    }


}
