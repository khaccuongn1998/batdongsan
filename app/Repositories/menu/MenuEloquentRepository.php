<?php


namespace App\Repositories\menu;


use App\Models\Menu;
use App\Repositories\EloquentRepository;

class MenuEloquentRepository extends EloquentRepository
{
    public function getModel()
    {
        // TODO: Implement getModel() method.
        return Menu::class;
    }

}
