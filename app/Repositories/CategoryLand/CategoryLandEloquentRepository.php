<?php


namespace App\Repositories\CategoryLand;


use App\Models\LandCategory;
use App\Repositories\EloquentRepository;

class CategoryLandEloquentRepository extends EloquentRepository
{
    public function getModel()
    {
        // TODO: Implement getModel() method.
        return LandCategory::class;
    }

}
