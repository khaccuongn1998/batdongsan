<?php


namespace App\Repositories\Slider;


use App\Models\Slider;
use App\Repositories\EloquentRepository;

class SliderEloquentRepository extends EloquentRepository
{
    public function getModel()
    {
        return Slider::class;
    }
}
