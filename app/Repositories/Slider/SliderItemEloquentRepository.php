<?php


namespace App\Repositories\Slider;


use App\Models\SliderItem;
use App\Repositories\EloquentRepository;

class SliderItemEloquentRepository extends EloquentRepository
{
    public function getModel()
    {
        return SliderItem::class;
    }
}
