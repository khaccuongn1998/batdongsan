<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PostCategory extends Model
{
    use Sluggable;
    use HasFactory;
    protected $fillable = [
        'name',
        'slug',
        'is_active'
    ];

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('images');
    }
}
