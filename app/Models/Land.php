<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\HasMedia;

class Land extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use Sluggable;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'content',
        'land_category_id',
        'is_viewed',
        'price',
        'address',
        'area',
        'location',
        'image',
    ];

    public function comments(){
        return $this->morphMany(Comment::class,'commentable');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('images');
    }
}
