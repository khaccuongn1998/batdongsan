<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Cviebrock\EloquentSluggable\Sluggable;


class Post extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use Sluggable;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'content',
        'post_category_id',
        'is_viewed',
        'image'
    ];

    public function category()
    {
        return $this->belongsTo(PostCategory::class);
    }

    public function comments(){
        return $this->morphMany(Comment::class,'commentable');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('images');
    }
}
