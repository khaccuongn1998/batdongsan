<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Slider extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable =
        [
            'name',
            'description',
            'slug',
        ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sliderItems(){
        return $this->hasMany(SliderItem::class);
    }

}
