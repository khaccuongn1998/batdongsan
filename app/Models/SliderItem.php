<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Cviebrock\EloquentSluggable\Sluggable;

class SliderItem extends Model implements HasMedia
{
    use HasFactory;
    use Sluggable;
    use InteractsWithMedia;
    protected $fillable =
        [
            'name',
            'description',
            'slug',
            'slider_id',
            'is_active',
            'image'
        ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getImageAttribute(){
        return $this->getFirstMediaUrl('images');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function slider(){
        return $this->belongsTo(Slider::class);
    }
}
