<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CategoryPost\CategoryPostEloquentRepository;

class AdminPostCategoryController extends Controller
{
    protected $postCategory;
    public function __construct(CategoryPostEloquentRepository $postCategory){
        $this->postCategory = $postCategory;
    }

    public function index(){
        $result = $this->postCategory->getAll();
        return view('admin.category-post.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->postCategory->getOne($id);
    }

    public function update($id,Request $request){
        $result = $this->postCategory->update($id,$request->all());
        return redirect()->route('admin.category-post.index');
    }

    public function delete($id){
        $result = $this->postCategory->delete($id);
    }

    public function add(){
        return view('admin.category-post.add');
    }

    public function edit($id){
        $result = $this->postCategory->getOne($id);
        return view('admin.category-post.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $result = $this->postCategory->create($request->all());
        return redirect()->back();
    }

}
