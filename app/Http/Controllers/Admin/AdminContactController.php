<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactEloquentRepository;
use Illuminate\Http\Request;

class AdminContactController extends Controller
{
    protected $contact;

    public function __construct(ContactEloquentRepository $contact)
    {
        $this->contact = $contact;
    }

    public function index()
    {
        $resultAll = $this->contact->getAll();
        return view('admin.contact.index', compact('resultAll'));
    }

    public function detail($id)
    {
        $result = $this->contact->getOne($id);
        $result->update(['is_read'=>1]);
        return view('admin.contact.detail', compact('result'));
    }

    public function delete($id)
    {
        $result = $this->contact->delete($id);
        if ($result) {
            return response()->json([
                'status' => 200,
                'message' => 'ok'
            ]);
        }
        return response()->json([
            'status' => 422,
            'message' => 'Error'
        ],422);

    }
}
