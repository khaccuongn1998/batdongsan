<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Comment\CommentEloquentRepository;
use Illuminate\Http\Request;

class AdminCommentController extends Controller
{
    protected $comment;
    public function __construct(CommentEloquentRepository $comment){
       $this->comment = $comment;
    }
    public function index(){
        return view('admin.comment.index');
    }
}
