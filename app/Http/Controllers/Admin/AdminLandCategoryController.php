<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryLand\CategoryLandEloquentRepository;
use Illuminate\Http\Request;

class AdminLandCategoryController extends Controller
{
    protected $landCategory;
    public function __construct(CategoryLandEloquentRepository $landCategory){
        $this->landCategory = $landCategory;
    }

    public function index(){
        $result = $this->landCategory->getAll();
        return view('admin.land-category.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->landCategory->getOne($id);
    }

    public function update($id,Request $request){
        $result = $this->landCategory->update($id,$request->all());
        return redirect()->route('admin.land-category.index');
    }

    public function delete($id){
        $result = $this->landCategory->delete($id);
    }

    public function add(){
        return view('admin.land-category.add');
    }

    public function edit($id){
        $result = $this->landCategory->getOne($id);
        return view('admin.land-category.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $result = $this->landCategory->create($request->all());
        return redirect()->back();
    }
}
