<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryProject\CategoryProjectEloquentRepository;
use Illuminate\Http\Request;

class AdminProjectCategoryController extends Controller
{
    protected $projectCategory;
    public function __construct(CategoryProjectEloquentRepository $projectCategory){
        $this->projectCategory = $projectCategory;
    }

    public function index(){
        $result = $this->projectCategory->getAll();
        return view('admin.project-category.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->projectCategory->getOne($id);
    }

    public function update($id,Request $request){
        $result = $this->projectCategory->update($id,$request->all());
        return redirect()->route('admin.project-category.index');
    }

    public function delete($id){
        $result = $this->projectCategory->delete($id);
    }

    public function add(){
        return view('admin.project-category.add');
    }

    public function edit($id){
        $result = $this->projectCategory->getOne($id);
        return view('admin.project-category.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $result = $this->projectCategory->create($request->all());
        return redirect()->back();
    }
}
