<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Slider\SliderEloquentRepository;
use Illuminate\Http\Request;

class AdminSliderController extends Controller
{
    protected $slider;
    public function __construct(SliderEloquentRepository $slider){
        return $this->slider = $slider;
    }

    public function index(){
        $result = $this->slider->getAll();
        return view('admin.slider.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->slider->getOne($id);
    }

    public function update($id,Request $request){
        $result = $this->slider->update($id,$request->all());
        return redirect()->route('admin.slider.index');
    }

    public function delete($id){
        $result = $this->slider->delete($id);
    }

    public function add(){
        return view('admin.slider.add');
    }

    public function edit($id){
        $result = $this->slider->getOne($id);
        return view('admin.slider.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $result = $this->slider->create($request->all());
        return redirect()->back();
    }

}
