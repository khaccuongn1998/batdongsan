<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Slider\SliderItemEloquentRepository;
use Illuminate\Http\Request;

class AdminSliderItemController extends Controller
{
    protected $sliderItem;
    public function __construct(SliderItemEloquentRepository $sliderItem){
        return $this->sliderItem = $sliderItem;
    }

    public function index(){
        $result = $this->sliderItem->getAll();
        return view('admin.slider-item.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->sliderItem->getOne($id);
    }

    public function update($id,Request $request){
        $data = $request->except('image');
        $result = $this->sliderItem->update($id,$data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->media()->delete($result->id);
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('admin.slider-item.index');
    }

    public function delete($id){
        $result = $this->sliderItem->delete($id);
        if($result)
        {
            return response()->json([
                'status' =>200,
                'message' =>'ok'
            ]);
        }
        return response()->json([
            'status' =>422,
            'message' =>'error'
        ],422);
    }

    public function add(){
        return view('admin.slider-item.add');
    }

    public function edit($id){
        $result = $this->sliderItem->getOne($id);
        return view('admin.slider-item.edit',compact('result'));
    }

    public function create(Request $request){
        $data = $request->except('image');
        $result = $this->sliderItem->create($data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->back();
    }

}
