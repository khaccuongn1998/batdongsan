<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Project\EloquentProjectRepository;
class AdminProjectController extends Controller
{
    protected $project;
    public function __construct(EloquentProjectRepository $project){
        $this->project = $project;
    }

    public function index(){
        $result = $this->project->getAll();
        return view('admin.project.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->project->getOne($id);
    }

    public function update($id,Request $request){
        $data = $request->except('image');
        $result = $this->project->update($id,$data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->media()->delete($result->id);
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('admin.project.index');
    }

    public function delete($id){
        $result = $this->project->delete($id);
        if($result)
        {
            return response()->json([
                'status' =>200,
                'message' =>'ok'
            ]);
        }
        return response()->json([
            'status' =>422,
            'message' =>'error'
        ],422);
    }

    public function add(){
        return view('admin.project.add');
    }

    public function edit($id){
        $result = $this->project->getOne($id);
        return view('admin.project.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $data = $request->except('image');
        $result = $this->project->create($data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->back();
    }
}
