<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\menu\MenuEloquentRepository;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    protected $menu;
    public function __construct(MenuEloquentRepository $menu){
        $this->menu = $menu;
    }

    public function index(){
        $result = $this->menu->getAll();
        return view('admin.menu.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->menu->getOne($id);
    }

    public function update($id,Request $request){
        $result = $this->menu->update($id,$request->all());
        return redirect()->route('admin.menu.index');
    }

    public function delete($id){
        $result = $this->menu->delete($id);
    }

    public function add(){
        return view('admin.menu.add');
    }

    public function edit($id){
        $result = $this->menu->getOne($id);
        return view('admin.menu.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $result = $this->menu->create($request->all());
        return redirect()->back();
    }
}
