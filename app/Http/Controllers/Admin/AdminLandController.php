<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Land\EloquentLandRepository;
use Illuminate\Http\Request;

class AdminLandController extends Controller
{
    protected $land;
    public function __construct(EloquentLandRepository $land){
        $this->land = $land;
    }

    public function index(){
        $result = $this->land->getAll();
        return view('admin.land.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->land->getOne($id);
    }

    public function update($id,Request $request){
        $data = $request->except('image');
        $result = $this->land->update($id,$data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->media()->delete($result->id);
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('admin.land.index');
    }

    public function delete($id){
        $result = $this->land->delete($id);
        if($result)
        {
            return response()->json([
                'status' =>200,
                'message' =>'ok'
            ]);
        }
        return response()->json([
            'status' =>422,
            'message' =>'error'
        ],422);
    }

    public function add(){
        return view('admin.land.add');
    }

    public function edit($id){
        $result = $this->land->getOne($id);
        return view('admin.land.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $data = $request->except('image');
        $result = $this->land->create($data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->back();
    }
}
