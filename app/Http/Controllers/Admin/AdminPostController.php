<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Post\EloquentPostRepository;

class AdminPostController extends Controller
{
    protected $post;
    public function __construct(EloquentPostRepository $post){
        $this->post = $post;
    }

    public function index(){
        $result = $this->post->getAll();
        return view('admin.post.index',compact('result'));
    }

    public function getOne($id){
        $result = $this->post->getOne($id);
    }

    public function update($id,Request $request){
        $data = $request->except('image');
        $result = $this->post->update($id,$data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->media()->delete($result->id);
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->route('admin.post.index');
    }

    public function delete($id){
        $result = $this->post->delete($id);
        if($result)
        {
            return response()->json([
                'status' =>200,
                'message' =>'ok'
            ]);
        }
        return response()->json([
            'status' =>422,
            'message' =>'error'
        ],422);
    }

    public function add(){
        return view('admin.post.add');
    }

    public function edit($id){
        $result = $this->post->getOne($id);
        return view('admin.post.edit',compact('result'));
    }

    public function create(Request $request){
        $request->validate(['name'=>'required']);
        $data = $request->except('image');
        $result = $this->post->create($data);
        if($request->hasFile('image') && $request->file('image')->isValid()){
            $result->addMediaFromRequest('image')->toMediaCollection('images');
        }
        return redirect()->back();
    }
}
