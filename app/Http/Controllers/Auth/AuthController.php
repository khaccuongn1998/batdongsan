<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('admin.login');
    }

    public function login(request $request)
    {
        $credentials = $request->only('email', 'password');
        $rememberMe = $request->has('remember') ? true : false;
        if (Auth::attempt($credentials,$rememberMe)) {
            return redirect()->route('admin.home');
        }
        return redirect()->back()->withInput()->withErrors('sai mk');
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login.index');
    }
}
