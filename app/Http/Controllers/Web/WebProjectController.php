<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryProject\CategoryProjectEloquentRepository;
use App\Repositories\Project\EloquentProjectRepository;
use Illuminate\Http\Request;

class WebProjectController extends Controller
{
    protected $project;
    protected $projectCategory;

    public function __construct(EloquentProjectRepository $project, CategoryProjectEloquentRepository $projectCategory){
        $this->project = $project;
        $this->projectCategory = $projectCategory;
    }

    public function index(){
        $result = $this->project->getPaginate(6);
        return view('web.page.project.index',compact('result'));
    }

    public function detail($slug){
        $result = $this->project->getBySlug($slug);
        return view('web.page.project.detail',compact('result'));
    }
}
