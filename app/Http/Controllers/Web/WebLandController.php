<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\CategoryLand\CategoryLandEloquentRepository;
use App\Repositories\Land\EloquentLandRepository;
use Illuminate\Http\Request;

class WebLandController extends Controller
{
    protected $land;
    protected $landCategory;

    public function __construct(EloquentLandRepository $land, CategoryLandEloquentRepository $landCategory){
        $this->land = $land;
        $this->landCategory = $landCategory;
    }

    public function index(){
        $result = $this->land->getPaginate(1);
        return view('web.page.land.index',compact('result'));
    }

    public function detail($slug)
    {
        $result = $this->land->getBySlug($slug);
        return view('web.page.land.detail', compact('result'));
    }
}
