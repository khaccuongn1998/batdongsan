<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactEloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WebContactController extends Controller
{
    protected $contact;
    public function __construct(ContactEloquentRepository $contact){
        $this->contact = $contact;
    }

    public function index(){
        return view('web.page.contact.index');
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(),[
            'name'=>'required', 'email'=>'required','phone'=>'required', 'g-recaptcha-response' => 'required',
            ]);
        if($validator->fails()){
            return response()->json([
                'status' =>422,
                'message' => 'Error'
            ],422);
        }
        $result = $this->contact->create($request->all());
        if($result){
            return response()->json([
                'status' => 200,
                'message' =>'Cảm ơn bạn đã liên hệ, chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất'
            ],200);
        }
        return response()->json([
            'status' => 422,
            'message' =>'Có lỗi xảy ra!!'
        ],422);
    }
}
