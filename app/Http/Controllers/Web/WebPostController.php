<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\Post\EloquentPostRepository;
use App\Repositories\CategoryPost\CategoryPostEloquentRepository;
use Illuminate\Http\Request;
use function Symfony\Component\Translation\t;

class WebPostController extends Controller
{
    protected $post;
    protected $postCategory;

    public function __construct(EloquentPostRepository $post, CategoryPostEloquentRepository $postCategory){
        $this->post = $post;
        $this->postCategory = $postCategory;
    }

    public function index(){
        $result = $this->post->getPaginate(6);
        return view('web.page.post.index',compact('result'));
    }

    public function detail($slug){
        $result = $this->post->getBySlug($slug);
        return view('web.page.post.detail',compact('result'));
    }
}
