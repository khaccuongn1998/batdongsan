<?php

use App\Models\Contact;
use App\Models\Land;
use App\Models\LandCategory;
use App\Models\PostCategory;
use App\Models\Post;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\Slider;


if(!function_exists('getNamePostCategoryById')){
    function getNamePostCategoryById($id) {
          $data = PostCategory::find($id);
         return  $data ? $data->name : null;
    }
}

if(!function_exists('getNameProjectCategoryById')){
    function getNameProjectCategoryById($id) {
        $data = ProjectCategory::find($id);
        return  $data ? $data->name : null;
    }
}

if(!function_exists('getNameLandCategoryById')){
    function getNameLandCategoryById($id) {
        $data = LandCategory::find($id);
        return  $data ? $data->name : null;
    }
}

if(!function_exists('getNameSliderCategoryById')){
    function getNameSliderCategoryById($id) {
        $data = Slider::find($id);
        return  $data ? $data->name : null;
    }
}

if (!function_exists('getAllPostCategory')){
    function getAllPostCategory(){
        return PostCategory::where('is_active',1)->get();
    }
}

if (!function_exists('getAllProjectCategory')){
    function getAllProjectCategory(){
        return ProjectCategory::where('is_active',1)->get();

    }
}

if (!function_exists('getAllLandCategory')){
    function getAllLandCategory(){
        return LandCategory::where('is_active',1)->get();

    }
}

if (!function_exists('getAllSliderCategory')){
    function getAllSliderCategory(){
        return Slider::all();

    }
}

if (!function_exists('getAllContact')){
    function getAllContact(){
        return Contact::all();

    }
}

if (!function_exists('getHomeBanner')){
    function getSlider($slug){
        $banner = Slider::where('slug',$slug)->first();
        $sliderItem = $banner->sliderItems();
        return $sliderItem;
    }
}

if (!function_exists('getHotProject')){
    function getHotProject(){
        $data = Project::orderBy('id', 'desc')->take(8)->get();
        return $data;
    }
}

if(!function_exists('getLastBlog')){
    function getLastBlogLimit($limit=3){
        $data = Post::orderBy('id', 'desc')->take($limit)->get();
        return $data;
    }
}

if(!function_exists('getLastLand')){
    function getLastLandLimit($limit=3){
        $data = Land::orderBy('id', 'desc')->take($limit)->get();
        return $data;
    }
}
