<?php
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\{
    AdminCommentController,
    AdminContactController,
    AdminProjectCategoryController,
    AdminProjectController,
    AdminSliderController,
    MenuController,
    AdminSliderItemController,
    AdminPostCategoryController,
    AdminPostController,
    AdminController,
    AdminLandCategoryController,
    AdminLandController,

};

use App\Http\Controllers\Auth\AuthController;


Route::get('login',[AuthController::class,'index'])->name('admin.login.index');
Route::post('check',[AuthController::class,'login'])->name('admin.login.check');

Route::middleware('auth:web')->group(function(){
    Route::get('logout',[AuthController::class,'logout'])->name('admin.logout');
    Route::get('/',[AdminController::class, 'index'])->name('admin.home');

    Route::prefix('category-post')->group(function(){
        Route::get('/',[AdminPostCategoryController::class,'index'])->name('admin.category-post.index');
        Route::get('/add',[AdminPostCategoryController::class,'add'])->name('admin.category-post.add');
        Route::post('/create',[AdminPostCategoryController::class,'create'])->name('admin.category-post.create');
        Route::get('/edit/{id}',[AdminPostCategoryController::class,'edit'])->name('admin.category-post.edit');
        Route::post('/update/{id}',[AdminPostCategoryController::class,'update'])->name('admin.category-post.update');
        Route::get('/delete/{id}',[AdminPostCategoryController::class,'delete'])->name('admin.category-post.delete');

    });

    Route::prefix('post')->group(function(){
        Route::get('/',[AdminPostController::class,'index'])->name('admin.post.index');
        Route::get('/add',[AdminPostController::class,'add'])->name('admin.post.add');
        Route::post('/create',[AdminPostController::class,'create'])->name('admin.post.create');
        Route::get('/edit/{id}',[AdminPostController::class,'edit'])->name('admin.post.edit');
        Route::post('/update/{id}',[AdminPostController::class,'update'])->name('admin.post.update');
        Route::get('/delete/{id}',[AdminPostController::class,'delete'])->name('admin.post.delete');

    });

    Route::prefix('project-category')->group(function(){
        Route::get('/',[AdminProjectCategoryController::class,'index'])->name('admin.project-category.index');
        Route::get('/add',[AdminProjectCategoryController::class,'add'])->name('admin.project-category.add');
        Route::post('/create',[AdminProjectCategoryController::class,'create'])->name('admin.project-category.create');
        Route::get('/edit/{id}',[AdminProjectCategoryController::class,'edit'])->name('admin.project-category.edit');
        Route::post('/update/{id}',[AdminProjectCategoryController::class,'update'])->name('admin.project-category.update');
        Route::get('/delete/{id}',[AdminProjectCategoryController::class,'delete'])->name('admin.project-category.delete');

    });

    Route::prefix('project')->group(function(){
        Route::get('/',[AdminProjectController::class,'index'])->name('admin.project.index');
        Route::get('/add',[AdminProjectController::class,'add'])->name('admin.project.add');
        Route::post('/create',[AdminProjectController::class,'create'])->name('admin.project.create');
        Route::get('/edit/{id}',[AdminProjectController::class,'edit'])->name('admin.project.edit');
        Route::post('/update/{id}',[AdminProjectController::class,'update'])->name('admin.project.update');
        Route::get('/delete/{id}',[AdminProjectController::class,'delete'])->name('admin.project.delete');

    });

    Route::prefix('land-category')->group(function(){
        Route::get('/',[AdminLandCategoryController::class,'index'])->name('admin.land-category.index');
        Route::get('/add',[AdminLandCategoryController::class,'add'])->name('admin.land-category.add');
        Route::post('/create',[AdminLandCategoryController::class,'create'])->name('admin.land-category.create');
        Route::get('/edit/{id}',[AdminLandCategoryController::class,'edit'])->name('admin.land-category.edit');
        Route::post('/update/{id}',[AdminLandCategoryController::class,'update'])->name('admin.land-category.update');
        Route::get('/delete/{id}',[AdminLandCategoryController::class,'delete'])->name('admin.land-category.delete');

    });

    Route::prefix('land')->group(function(){
        Route::get('/',[AdminLandController::class,'index'])->name('admin.land.index');
        Route::get('/add',[AdminLandController::class,'add'])->name('admin.land.add');
        Route::post('/create',[AdminLandController::class,'create'])->name('admin.land.create');
        Route::get('/edit/{id}',[AdminLandController::class,'edit'])->name('admin.land.edit');
        Route::post('/update/{id}',[AdminLandController::class,'update'])->name('admin.land.update');
        Route::get('/delete/{id}',[AdminLandController::class,'delete'])->name('admin.land.delete');

    });

    Route::prefix('slider')->group(function(){
        Route::get('/',[AdminSliderController::class,'index'])->name('admin.slider.index');
        Route::get('/add',[AdminSliderController::class,'add'])->name('admin.slider.add');
        Route::post('/create',[AdminSliderController::class,'create'])->name('admin.slider.create');
        Route::get('/edit/{id}',[AdminSliderController::class,'edit'])->name('admin.slider.edit');
        Route::post('/update/{id}',[AdminSliderController::class,'update'])->name('admin.slider.update');
        Route::get('/delete/{id}',[AdminSliderController::class,'delete'])->name('admin.slider.delete');

    });

    Route::prefix('slider-item')->group(function(){
        Route::get('/',[AdminSliderItemController::class,'index'])->name('admin.slider-item.index');
        Route::get('/add',[AdminSliderItemController::class,'add'])->name('admin.slider-item.add');
        Route::post('/create',[AdminSliderItemController::class,'create'])->name('admin.slider-item.create');
        Route::get('/edit/{id}',[AdminSliderItemController::class,'edit'])->name('admin.slider-item.edit');
        Route::post('/update/{id}',[AdminSliderItemController::class,'update'])->name('admin.slider-item.update');
        Route::get('/delete/{id}',[AdminSliderItemController::class,'delete'])->name('admin.slider-item.delete');

    });

    Route::prefix('menu')->group(function(){
        Route::get('/',[MenuController::class,'index'])->name('admin.menu.index');
        Route::get('/add',[MenuController::class,'add'])->name('admin.menu.add');
        Route::post('/create',[MenuController::class,'create'])->name('admin.menu.create');
        Route::get('/edit/{id}',[MenuController::class,'edit'])->name('admin.menu.edit');
        Route::post('/update/{id}',[MenuController::class,'update'])->name('admin.menu.update');
        Route::get('/delete/{id}',[MenuController::class,'delete'])->name('admin.menu.delete');

    });

    Route::prefix('contact')->group(function(){
        Route::get('/',[AdminContactController::class,'index'])->name('admin.contact.index');
        Route::get('/detail/{id}',[AdminContactController::class,'detail'])->name('admin.contact.detail');
        Route::get('/delete/{id}',[AdminContactController::class,'delete'])->name('admin.contact.delete');

    });

    Route::prefix('comment')->group(function(){
        Route::get('/',[AdminCommentController::class,'index'])->name('admin.comment.index');
        Route::get('/delete/{id}',[AdminCommentController::class,'delete'])->name('admin.comment.delete');

    });

});
