<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\{HomeController, WebContactController, WebLandController, WebPostController, WebProjectController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');

Route::get('/gioi-thieu', [HomeController::class, 'about'])->name('about');

Route::prefix('/du-an')->group(function () {
    Route::get('/',[WebProjectController::class, 'index'])->name('project.index');
    Route::get('/{slug}', [WebProjectController::class,'detail'])->name('project.detail');
});

Route::prefix('/tin-tuc')->group(function(){
    Route::get('/',[WebPostController::class, 'index'])->name('post.index');
    Route::get('/{slug}', [WebPostController::class,'detail'])->name('post.detail');
});

Route::prefix('/dat-ban')->group(function(){
    Route::get('/',[WebLandController::class, 'index'])->name('land.index');
    Route::get('/{slug}.html', [WebLandController::class,'detail'])->name('land.detail');
});

Route::prefix('lien-he')->group(function(){
    Route::get('/',[WebContactController::class, 'index'])->name('contact.index');
    Route::post('/store',[WebContactController::class, 'store'])->name('contact.create');
});
